pipelineJob('pipelineJob') {
    definition {
        cps {
            script(readFileFromWorkspace('pipelineJob.groovy'))
            sandbox()
        }
    }
}

pipelineJob('theme-park-job') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url 'https://Alisao12@bitbucket.org/Alisao12/theme-park-rides.git'
                    }
                    branch 'master'
                }
            }
        }
    }
}